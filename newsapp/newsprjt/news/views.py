import django.shortcuts
from newsapi import NewsApiClient

def index(request):
    newsapi=NewsApiClient(api_key="cac8eaf49f0342608ffca5677737daf0")
    topheadlines=newsapi.get_top_headlines(sources='al-jazeera-english')
    articles=topheadlines['articles']
    desc=[]
    news=[]
    img=[]
    for i in range(len(articles)):
        myarticles=articles[i]
        news.append(myarticles['title'])
        desc.append(myarticles['description'])
        img.append(myarticles['urlToImage'])
    mylist=zip(news,desc,img)
    return django.shortcuts.render(request, 'index.html', context={"mylist":mylist})
def bbc(request):
    newsapi=NewsApiClient(api_key="cac8eaf49f0342608ffca5677737daf0")
    topheadlines=newsapi.get_top_headlines(sources='bbc-news')
    articles=topheadlines['articles']
    desc=[]
    news=[]
    img=[]
    for i in range(len(articles)):
        myarticles=articles[i]
        news.append(myarticles['title'])
        desc.append(myarticles['description'])
        img.append(myarticles['urlToImage'])
    mylist=zip(news,desc,img)
    return django.shortcuts.render(request, 'bbc.html', context={"mylist":mylist})